/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class LightUpdateListener
 */
import { Listener, ListenerInterface } from "nest-async-event";
import { LightUpdateEvent } from "~/module/domain/light/event/LightUpdateEvent";
import { MqttClient } from "mqtt";

@Listener(LightUpdateEvent.name)
export class LightUpdateListener implements ListenerInterface {

    constructor(
        private readonly client: MqttClient
    ) {}

    public async listen(event: LightUpdateEvent): Promise<void> {
        try {
            const light = event.target;
            this.client.publish(`v1/light/${light.getSerialNumber()}`, String(Number(light.isOn)));
        }  catch (e) {}
    }
}
