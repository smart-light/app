import { ClassSerializerInterceptor, HttpModule, Module, ValidationPipe } from '@nestjs/common';
import { APP_INTERCEPTOR, APP_PIPE } from "@nestjs/core";
import * as mqtt from "mqtt";
import { DatabaseModule } from "~/module/infrastructure/database/DatabaseModule";
import { LightHttpModule } from "~/module/domain-service/light-http/LightHttpModule";
import { SubscriptionHttpModule } from "~/module/domain-service/subscription-http/SubscriptionHttpModule";
import { UserHttpModule } from "~/module/domain-service/user-http/UserHttpModule";
import { RoomHttpModule } from "~/module/domain-service/room-http/RoomHttpModule";
import { NestAsyncEventModule } from "nest-async-event";
import { LightUpdateListener } from "~/listener/LightUpdateListener";
import { BrownieModule } from "~/module/brownie/BrownieModule";
import { MailerModule } from "@nestjs-modules/mailer";
import { TelegramModule } from "nestjs-telegram";

@Module({
  imports: [
      LightHttpModule,
      SubscriptionHttpModule,
      UserHttpModule,
      RoomHttpModule,
      BrownieModule,
      NestAsyncEventModule.forRoot(),
      {
          global: true,
          ...TelegramModule.forRoot({
              botKey: process.env.TELEGRAM_BOT_KEY
          }),
      },
      DatabaseModule.forRoot(process.env.MONGO_URI, {
        useNewUrlParser: true,
        authMechanism: "DEFAULT",
        authSource: process.env.MONGO_AUTH_SOURCE,
        auth: {
          user: process.env.MONGO_USER,
          password: process.env.MONGO_PASS
        },
      }),
      MailerModule.forRoot({
          transport:  {
              port: Number(process.env.SMTP_PORT),
              host: process.env.SMTP_HOST,
              auth: {
                  user: process.env.SMTP_ADDR,
                  pass: process.env.SMTP_PASS
              }
          },
          defaults: {
              from: process.env.SMTP_ADDR,
          },
      }),
  ],
  providers: [
      LightUpdateListener,
      {
        provide: mqtt.MqttClient,
        useValue: mqtt.connect(`${process.env.MQTT_URI}`, {
          clientId: `${new Date().getTime()}-application`,
          resubscribe: true,
          username: process.env.MQTT_USERNAME,
          password: process.env.MQTT_PASSWORD
        })
      },
      {
        provide: APP_INTERCEPTOR,
        useClass: ClassSerializerInterceptor,
      },
      {
        provide: APP_PIPE,
        useValue: new ValidationPipe({
          transform: true
        })
      }
  ],
})
export class AppModule {
}
