/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class UserDto
 */
import { UserData as TUserData } from "~/module/domain/user/Types";
import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export class UserDto implements TUserData {
    @ApiProperty()
    @IsString()
    email: string;
}
