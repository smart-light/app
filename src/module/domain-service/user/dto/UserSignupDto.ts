/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class UserSignupData
 */
import { UserSignupData } from "~/module/domain/user/Types";
import { UserDto } from "~/module/domain-service/user/dto/UserDto";
import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export class UserSignupDto extends UserDto implements UserSignupData {
    @ApiProperty()
    @IsString()
    password: string;
}
