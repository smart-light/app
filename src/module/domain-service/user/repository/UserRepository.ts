/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class UserRepository
 */
import { DataStore } from "~/module/infrastructure/database/DataStore";
import { Collection, ObjectId } from "mongodb";
import { UserSchema } from "~/module/domain-service/user/schema/UserSchema";
import { User, UserStorageInterface } from "~/module/domain/user/Types";
import { Injectable } from "@nestjs/common";
import { FindOptions } from "~/module/domain-service/user/options/FindOptions";

@Injectable()
export class UserRepository implements UserStorageInterface {

    private readonly collection: Collection = this.dataStore.getCollection(UserSchema);

    constructor(
        private readonly dataStore: DataStore
    ) {}

    public async findBy(options: FindOptions = {}): Promise<UserSchema[]> {
        const filter = {} as Record<string, any>;

        if(options.ids) {
            filter["_id"] = {
                $in: options.ids.map(id => new ObjectId(id))
            }
        }

        return this.dataStore.findBy(UserSchema, filter);
    }

    public async get(id: string): Promise<UserSchema> {
        return this.dataStore.get(id, UserSchema);
    }

    public async remove(user: User): Promise<void> {
        await this.dataStore.remove(
            await this.get(user.id)
        );
    }

    public async save(user: User): Promise<void> {
        user = new UserSchema(user);
        await this.dataStore.save(user);
    }

    public async update(user: User): Promise<void> {
        user = new UserSchema(user);
        await this.dataStore.update(user);
    }


}
