/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class UserService
 */
import { UserRepository } from "~/module/domain-service/user/repository/UserRepository";
import { Injectable } from "@nestjs/common";
import { UserSignupData } from "~/module/domain/user/Types";
import * as UserDomain from "~/module/domain/user/UserDomain";
import { UserSchema } from "~/module/domain-service/user/schema/UserSchema";
import { ObjectId } from "mongodb";

@Injectable()
export class UserService {
    constructor(
        private readonly userRepository: UserRepository
    ) {}

    public async signup(data: UserSignupData): Promise<UserSchema> {
        const result = await UserDomain.signup((new ObjectId().toString()), this.userRepository, data);

        return new UserSchema(result.user);
    }

    public async remove(id: string): Promise<void> {
        await UserDomain.remove(id, this.userRepository);
    }
}
