/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class UserModule
 */
import { Module } from "@nestjs/common";
import { UserRepository } from "~/module/domain-service/user/repository/UserRepository";
import { UserService } from "~/module/domain-service/user/service/UserService";

@Module({
    providers: [
        UserRepository,
        UserService
    ],
    exports: [
        UserRepository,
        UserService
    ]
})
export class UserModule {
}
