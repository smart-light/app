/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class UserSchema
 */
import { Embedded, Entity, Id, Property } from "seth-odm";
import { Exclude, Expose } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";
import { User as TUser } from "~/module/domain/user/Types";

@Entity('users')
export class UserSchema implements TUser {
    @Id()
    @Expose()
    @ApiProperty()
    readonly id?: string;

    @Property()
    @Expose()
    @ApiProperty()
    readonly email: string;

    @Embedded()
    @Exclude()
    readonly password: string;

    constructor(user: TUser) {
        this.id = user.id;
        this.email = user.email;
        this.password = user.password;
    }
}

