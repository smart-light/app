/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class LightHttpModule
 */
import { Module } from "@nestjs/common";
import { LightModule } from "~/module/domain/light/LightModule";
import { LightController } from "~/module/domain-service/light-http/controller/LightController";

@Module({
    controllers: [
        LightController,
    ],
    imports: [
        LightModule
    ],
    exports: [
        LightModule,
    ]
})
export class LightHttpModule {
}
