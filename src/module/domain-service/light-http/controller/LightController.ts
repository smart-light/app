/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class LightController
 */
import { Body, Controller, Get, Post, Put } from "@nestjs/common";
import { LightService } from "~/module/domain/light/service/LightService";
import { ApiResponse, ApiTags } from "@nestjs/swagger";
import { Light } from "~/module/domain/light/entity/Light";
import { UpdateLightData } from "~/module/domain/light/dto/UpdateLightData";
import { CreateLightData } from "~/module/domain/light/dto/CreateLightData";

@Controller("api/v1")
@ApiTags("Light")
export class LightController {

    constructor(
        private readonly lightService: LightService
    ) {}

    @Get("/lights")
    @ApiResponse({type: Light, isArray: true, status: 200})
    public async list(): Promise<Light[]> {
        return this.lightService.list();
    }

    @Post("/light")
    @ApiResponse({type: Light, status: 201})
    public async create(@Body() data: CreateLightData): Promise<Light> {
        return this.lightService.create(data);
    }

    @Put("/light")
    @ApiResponse({type: Light, status: 200})
    public async update(@Body() data: UpdateLightData): Promise<Light> {
        return this.lightService.update(data);
    }

}
