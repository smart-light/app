/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class SubscriptionHttpModule
 */
import { Module } from "@nestjs/common";
import { SubscriptionModule } from "~/module/domain/subscription/SubscriptionModule";
import { SubscriptionController } from "~/module/domain-service/subscription-http/controller/SubscriptionController";

@Module({
    controllers: [
        SubscriptionController
    ],
    imports: [
        SubscriptionModule
    ]
})
export class SubscriptionHttpModule {
}
