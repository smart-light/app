/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class SubscriptionController
 */
import { Body, Controller, Delete, Param, Post, Put } from "@nestjs/common";
import { SubscriptionService } from "~/module/domain/subscription/service/SubscriptionService";
import { Subscription } from "~/module/domain/subscription/entity/Subscription";
import { CreateSubscriptionData } from "~/module/domain/subscription/dto/CreateSubscriptionData";
import { UpdateSubscriptionData } from "~/module/domain/subscription/dto/UpdateSubscriptionData";
import { ApiCreatedResponse, ApiOkResponse, ApiResponse, ApiTags } from "@nestjs/swagger";

@Controller("/api/v1")
@ApiTags("Subscription")
export class SubscriptionController {
    constructor(
        private readonly subscriptionService: SubscriptionService
    ) {}

    @Post("/subscription")
    @ApiCreatedResponse({type: Subscription})
    public async create(@Body() data: CreateSubscriptionData): Promise<Subscription> {
        return this.subscriptionService.create(data);
    }

    @Put("/subscription")
    @ApiOkResponse({type: Subscription})
    public async update(@Body() data: UpdateSubscriptionData): Promise<Subscription> {
        return this.subscriptionService.update(data);
    }

    @Delete("/subscription/:id")
    @ApiOkResponse()
    public async remove(@Param("id") id: string): Promise<void> {
        await this.subscriptionService.remove(id);
    }
}
