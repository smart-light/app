/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class RoomController
 */
import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { ApiCreatedResponse, ApiOkResponse, ApiResponse, ApiTags } from "@nestjs/swagger";
import { RoomService } from "~/module/domain/room/service/RoomService";
import { Room } from "~/module/domain/room/entity/Room";
import { RoomDto } from "~/module/domain/room/dto/RoomDto";

@Controller("api/v1")
@ApiTags("Room")
export class RoomController {
    constructor(
        private readonly roomService: RoomService
    ) {}

    @Get("rooms")
    @ApiOkResponse({type: Room, isArray: true})
    public async list(): Promise<Room[]> {
        return this.roomService.list();
    }

    @Post("room")
    @ApiCreatedResponse({type: Room})
    public async create(@Body() data: RoomDto): Promise<Room> {
        return this.roomService.create(data);
    }

    @Put("room/:id")
    @ApiResponse({type: Room})
    public async update(@Param() id: string, @Body() data: RoomDto): Promise<Room> {
        return this.roomService.update(id, data);
    }

    @Delete("room/:id")
    @ApiResponse({status: 200})
    public async remove(@Param() id: string): Promise<void> {
        await this.roomService.remove(id);
    }
}
