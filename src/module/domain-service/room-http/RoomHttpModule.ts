/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class RoomHttpModule
 */
import { Module } from "@nestjs/common";
import { RoomModule } from "~/module/domain/room/RoomModule";
import { RoomController } from "~/module/domain-service/room-http/controller/RoomController";

@Module({
    controllers: [
        RoomController
    ],
    imports: [
        RoomModule
    ],
    exports: [
        RoomModule
    ]
})
export class RoomHttpModule {
}
