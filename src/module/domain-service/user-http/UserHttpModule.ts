/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class UserHttpModule
 */
import { Module } from "@nestjs/common";
import { UserModule } from "~/module/domain-service/user/UserModule";
import { UserController } from "~/module/domain-service/user-http/controller/UserController";

@Module({
    controllers: [
        UserController
    ],
    imports: [
        UserModule,
    ],
    exports: [
        UserModule
    ]
})
export class UserHttpModule {
}
