/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class UserController
 */
import { Body, Controller, Delete, Param, Post } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { UserService } from "~/module/domain-service/user/service/UserService";
import { UserSchema } from "~/module/domain-service/user/schema/UserSchema";
import { UserSignupDto } from "~/module/domain-service/user/dto/UserSignupDto";

@Controller("api/v1")
@ApiTags("User")
export class UserController {
    constructor(
        private readonly userService: UserService
    ) {}


    @Post("user/signup")
    public async signup(@Body() data: UserSignupDto): Promise<UserSchema> {
        return this.userService.signup(data);
    }

    @Delete("user/:id")
    public async remove(@Param("id") id: string): Promise<void> {
        return this.userService.remove(id);
    }
}
