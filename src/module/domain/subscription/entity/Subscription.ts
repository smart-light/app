/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Subscription
 */
import { Entity, Id, Property } from "seth-odm";
import { Expose } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

@Entity('subscriptions')
export class Subscription {

    @Id()
    @Expose()
    @ApiProperty()
    private readonly id: string;

    @Property()
    @Expose()
    @ApiProperty()
    private activationDate: Date = new Date();

    @Expose()
    @Property()
    @ApiProperty()
    private active: boolean = false;

    constructor(activationDate: Date = new Date()) {
        this.activationDate = activationDate;
    }

    public activate(): this {
        this.active = true;
        return this;
    }

    public disable(): this {
        this.active = false;
        return this;
    }

    public changeActivationDate(date: Date): void {
        this.activationDate = date;
    }
}
