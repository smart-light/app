/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class SubscriptionRepository
 */
import { Injectable } from "@nestjs/common";
import { DataStore } from "~/module/infrastructure/database/DataStore";
import { Collection, ObjectId } from "mongodb";
import { Subscription } from "~/module/domain/subscription/entity/Subscription";
import { FindOptions } from "~/module/domain/subscription/options/FindOptions";

@Injectable()
export class SubscriptionRepository {

    private readonly collection: Collection = this.dataStore.getCollection(Subscription);

    constructor(
        private readonly dataStore: DataStore
    ) {}

    public async findBy(options: FindOptions): Promise<Subscription[]> {
        const filter = {} as Record<string, any>;

        if(options.ids) {
            filter["_id"] = { $in: options.ids.map(id => new ObjectId(id)) };
        }

        return await this.dataStore.findBy(Subscription, filter);
    }

    public async save(subscription: Subscription): Promise<void> {
        await this.dataStore.save(subscription);
    }

    public async get(id: string): Promise<Subscription> {
        return await this.dataStore.get(id, Subscription);
    }

    public async update(subscription: Subscription): Promise<void> {
        await this.dataStore.update(subscription);
    }

    public async remove(subscription: Subscription): Promise<void> {
        await this.dataStore.remove(subscription);
    }

}
