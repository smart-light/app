/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class SubscriptionModule
 */
import { Module } from "@nestjs/common";
import { SubscriptionRepository } from "~/module/domain/subscription/repository/SubscriptionRepository";
import { SubscriptionService } from "~/module/domain/subscription/service/SubscriptionService";

@Module({
    providers: [
        SubscriptionRepository,
        SubscriptionService
    ],
    exports: [
        SubscriptionRepository,
        SubscriptionService
    ],
})
export class SubscriptionModule {
}
