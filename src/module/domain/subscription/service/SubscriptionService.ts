/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class SubscriptionService
 */
import { Injectable, NotImplementedException } from "@nestjs/common";
import { SubscriptionRepository } from "~/module/domain/subscription/repository/SubscriptionRepository";
import { Subscription } from "~/module/domain/subscription/entity/Subscription";
import { CreateSubscriptionData } from "~/module/domain/subscription/dto/CreateSubscriptionData";
import { UpdateSubscriptionData } from "~/module/domain/subscription/dto/UpdateSubscriptionData";

@Injectable()
export class SubscriptionService {
    constructor(
        private readonly subscriptionRepository: SubscriptionRepository
    ) {}

    public async create(data: CreateSubscriptionData): Promise<Subscription> {
        const subscription = new Subscription(data.activationDate);

        if(data.hasOwnProperty('active')) {
            data.active ? subscription.activate() : subscription.disable();
        }

        await this.subscriptionRepository.save(subscription);

        return subscription;
    }

    public async update(data: UpdateSubscriptionData): Promise<Subscription> {
        const subscription = await this.subscriptionRepository.get(data.id);

        if(data.activationDate !== undefined)
            subscription.changeActivationDate(data.activationDate);

        if(data.active !== undefined) {
            data.active ? subscription.activate() : subscription.disable();
        }

        await this.subscriptionRepository.update(subscription);

        return subscription;
    }

    public async remove(id: string): Promise<void> {
        await this.subscriptionRepository.remove(
            await this.subscriptionRepository.get(id)
        );
    }
}
