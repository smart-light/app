/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class CreateSubscriptionData
 */
import { SubscriptionData } from "~/module/domain/subscription/dto/SubscriptionData";

export class CreateSubscriptionData extends SubscriptionData {
}
