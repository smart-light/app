/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class SubscriptionData
 */
import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsDate, IsOptional, IsString } from "class-validator";
import { Type } from "class-transformer";

export abstract class SubscriptionData {
    @ApiProperty({
        name: 'activationDate',
        type: 'date'
    })
    @IsOptional()
    @IsDate()
    @Type(() => Date)
    activationDate?: Date;

    @ApiProperty({
        name: "active"
    })
    @IsOptional()
    @IsBoolean()
    active?: boolean;
}
