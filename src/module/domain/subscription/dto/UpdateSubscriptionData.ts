/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class UpdateSubscriptionData
 */
import { SubscriptionData } from "~/module/domain/subscription/dto/SubscriptionData";
import { IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateSubscriptionData extends SubscriptionData {
    @ApiProperty()
    @IsString()
    id: string;
}
