/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class FindOptions
 */
export class FindOptions {
    ids?: string[];
}
