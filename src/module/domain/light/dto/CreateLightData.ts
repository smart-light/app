/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class CreateLightData
 */
import { IsBoolean, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateLightData {
    @ApiProperty()
    @IsString()
    public serialNumber: string;

    @ApiProperty()
    @IsBoolean()
    public enable: boolean;
}
