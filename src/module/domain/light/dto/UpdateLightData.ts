/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class UpdateLightData
 */
import { IsBoolean, IsOptional, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateLightData {
    @ApiProperty({
        required: true,
        type: 'string',
        name: 'id'
    })
    @IsString()
    public id: string;

    @ApiProperty({
        required: false,
        name: 'serialNumber',
        type: 'string'
    })
    @IsString()
    @IsOptional()
    public serialNumber?: string;

    @ApiProperty({
        required: true,
        name: 'enable',
        type: 'boolean'
    })
    @IsBoolean()
    public enable?: boolean;
}
