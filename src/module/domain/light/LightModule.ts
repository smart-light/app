/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class LightModule
 */
import { Module } from "@nestjs/common";
import { LightRepository } from "~/module/domain/light/repository/LightRepository";
import { LightService } from "~/module/domain/light/service/LightService";

@Module({
    providers: [
        LightService,
        LightRepository
    ],
    exports: [
        LightService,
        LightRepository
    ]
})
export class LightModule {
}
