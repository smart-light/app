/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class LightRepository
 */
import { Light } from "../entity/Light";
import { Injectable } from "@nestjs/common";
import { Mapper } from "seth-odm";
import { Collection, ObjectId } from "mongodb";
import { DataStore } from "~/module/infrastructure/database/DataStore";
import { FindOptions } from "~/module/domain/light/options/FindOptions";


@Injectable()
export class LightRepository {

    private readonly collection: Collection = this.store.getCollection(Light);
    private readonly mapper: Mapper = new Mapper();

    constructor(
        private readonly store: DataStore
    ) {}

    public async findBy(options: FindOptions = {}): Promise<Light[]> {
        const filter = {} as Record<string, any>;

        if(options.ids !== undefined) {
            filter["_id"] = {
                $in: options.ids.map(id => new ObjectId(id))
            };
        }

        const docs = await this.collection.find(filter).toArray();

        return docs.map(doc => this.mapper.toClass(doc, Light));
    }

    public async save(light: Light) {
        const doc = this.mapper.toDocument(light).toObject();

        Reflect.set(light, "_id", String(doc._id));

        await this.collection.insertOne(doc);

        return light;
    }

    public async get(id: string): Promise<Light> {
        const result = await this.collection.findOne({ _id: new ObjectId(id) });

        if(!result) {
            throw new Error("Light not found");
        }

        return this.mapper.toClass(result, Light);
    }
}
