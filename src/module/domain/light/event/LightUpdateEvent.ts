/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class LightUpdateEvent
 */
import { EventInterface } from "nest-async-event";
import { Light } from "~/module/domain/light/entity/Light";

export class LightUpdateEvent implements EventInterface {
    readonly name: string = LightUpdateEvent.name;

    constructor(
        public readonly target: Light
    ) {}
}
