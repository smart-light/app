/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class LightService
 */
import { Injectable } from "@nestjs/common";
import { Light } from "../entity/Light";
import { LightRepository } from "~/module/domain/light/repository/LightRepository";
import { CreateLightData } from "~/module/domain/light/dto/CreateLightData";
import { UpdateLightData } from "~/module/domain/light/dto/UpdateLightData";
import { EventEmitterInterface, InjectEventEmitter } from "nest-async-event";
import { LightUpdateEvent } from "~/module/domain/light/event/LightUpdateEvent";

@Injectable()
export class LightService {

    constructor(
        private readonly lightRepository: LightRepository,
        @InjectEventEmitter() private readonly eventEmitter: EventEmitterInterface
    ) {}

    public async create(data: CreateLightData): Promise<Light> {
        const light = new Light(data.serialNumber);

        await this.lightRepository.save(light);

        return light;
    }

    public async update(data: UpdateLightData): Promise<Light> {
        const light = await this.lightRepository.get(data.id);

        if(data.hasOwnProperty("serialNumber")) {
            light.changeSerialNumber(data.serialNumber);
        }
        data.enable ? light.on() : light.off();

        await this.eventEmitter.emit(new LightUpdateEvent(light));

        return light;
    }

    public async list(): Promise<Light[]> {
        return this.lightRepository.findBy();
    }
}
