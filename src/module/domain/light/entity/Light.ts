/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Light
 */
import { Exclude, Expose } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";
import { Entity, Id, Property } from "seth-odm";

@Entity('lights')
export class Light {
    @ApiProperty({
        name: 'id',
        type: 'string'
    })
    @Expose()
    @Id()
    private readonly id?: string;

    @Property()
    @Exclude()
    private enable: boolean;

    @Property()
    @Exclude()
    serialNumber: string;

    constructor(serialNumber: string) {
        this.serialNumber = serialNumber;
        this.enable = false;
    }

    public on(): void {
        this.enable = true;
    }

    public off() {
        this.enable = false;
    }

    public changeSerialNumber(sn: string) {
        this.serialNumber = sn;
    }

    @ApiProperty({
        name: 'serialNumber',
        type: 'string'
    })
    @Expose({name: "serialNumber"})
    public getSerialNumber(): string {
        return this.serialNumber;
    }

    @ApiProperty({
        name: 'isOff',
        type: 'boolean'
    })
    @Expose({name: "isOff"})
    public get isOff() {
        return this.enable === false;
    }

    @ApiProperty({
        name: 'isOn',
        type: 'boolean'
    })
    @Expose({name: "isOn"})
    public get isOn() {
        return this.enable === true;
    }
}
