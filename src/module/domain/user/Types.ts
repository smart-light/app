export type Nullable<T> = T | null;

export type Id = string;
export type Password = string;
export type Email = string;

export type User = {
    id?: Id;
    email: Email;
    password: Nullable<Password>;
}

export type UserData = {
    email: string;
};

export type UserSignupData = { password: string } & UserData;

export type SignupResult = { user: User };
export type RemoveResult = { user: User };

export interface UserStorageInterface {
    get(id: string): Promise<User>;
    save(user: User): Promise<void>;
    update(user: User): Promise<void>;
    remove(user: User): Promise<void>;
}
