import { Id, RemoveResult, SignupResult, User, UserSignupData, UserStorageInterface } from "~/module/domain/user/Types";

export const signup = async (id: Id, storage: UserStorageInterface, data: UserSignupData): Promise<SignupResult> => {
    const user: User = {
        id: id,
        email: data.email,
        password: data.password
    };

    await storage.save(user);

    return { user };
};

export const remove = async (id: string, storage: UserStorageInterface): Promise<RemoveResult> => {
    const user = await storage.get(id);

    await storage.remove(user);

    return { user }
}
