/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class RoomDto
 */
import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";

export class RoomDto {
    @ApiProperty({
        required: false,
        type: 'string',
        isArray: true
    })
    @IsOptional()
    @IsString({
        each: true
    })
    lights?: string[];

    @ApiProperty({
        type: 'string',
        required: false
    })
    @IsOptional()
    @IsString()
    subscription?: string;

    @ApiProperty({
        type: 'string',
        isArray: true,
        required: false
    })
    @IsOptional()
    @IsString({
        each: true
    })
    users?: string[];

    @ApiProperty({required: true})
    @IsString()
    title: string;

    @ApiProperty({required: true})
    @IsString()
    name: string;
}
