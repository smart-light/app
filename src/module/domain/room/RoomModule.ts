/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class RoomModule
 */
import { Module } from "@nestjs/common";
import { RoomService } from "~/module/domain/room/service/RoomService";
import { RoomRepository } from "~/module/domain/room/repository/RoomRepository";
import { LightModule } from "~/module/domain/light/LightModule";
import { SubscriptionModule } from "~/module/domain/subscription/SubscriptionModule";
import { UserModule } from "~/module/domain-service/user/UserModule";

@Module({
    imports: [
        LightModule,
        SubscriptionModule,
        UserModule
    ],
    providers: [
        RoomService,
        RoomRepository
    ],
    exports: [
        RoomService,
        RoomRepository
    ]
})
export class RoomModule {
}
