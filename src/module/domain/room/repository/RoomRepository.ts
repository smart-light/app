/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class RoomRepository
 */
import { DataStore } from "~/module/infrastructure/database/DataStore";
import { Room } from "~/module/domain/room/entity/Room";
import { Injectable } from "@nestjs/common";

@Injectable()
export class RoomRepository {
    constructor(
        private readonly dataStore: DataStore
    ) {}

    public async findBy(): Promise<Room[]> {
        return this.dataStore.findBy(Room, {});
    }

    public async save(room: Room): Promise<void> {
        await this.dataStore.save(room);
    }

    public async get(id: string): Promise<Room> {
        return this.dataStore.get(id, Room);
    }

    public async remove(room: Room): Promise<void> {
        await this.dataStore.remove(room);
    }

    public async update(room: Room): Promise<void> {
        await this.dataStore.update(room);
    }
}
