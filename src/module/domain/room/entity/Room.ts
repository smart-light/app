/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Room
 */
import { Entity, Id, Property, Reference } from "seth-odm";
import { Light } from "~/module/domain/light/entity/Light";
import { UserSchema } from "~/module/domain-service/user/schema/UserSchema";
import { Subscription } from "~/module/domain/subscription/entity/Subscription";
import { Exclude, Expose } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

@Entity('rooms')
export class Room {
    @Id()
    @Exclude()
    private readonly id?: string;

    @Property()
    @Exclude()
    private readonly name: string;

    @Property()
    @Exclude()
    private readonly title: string;

    @Reference(Light)
    @Exclude()
    private lights: Light[] = [];

    @Reference(UserSchema)
    @Exclude()
    private users: UserSchema[] = [];

    @Reference()
    @Exclude()
    private subscription: Subscription;

    constructor(name: string, title: string) {
        this.name = name;
        this.title = title;
    }

    @ApiProperty({name: 'id', type: 'string'})
    @Expose({name: 'id'})
    public getId() {
        return this.id || null;
    }

    @ApiProperty({name: 'name', type: 'string'})
    @Expose({name: 'name'})
    public getName() {
        return this.name;
    }

    @ApiProperty({name: 'title', type: 'string'})
    @Expose({name: 'title'})
    public getTitle() {
        return this.title;
    }

    @ApiProperty({name: 'lights', type: Light, isArray: true})
    @Expose({name: 'lights'})
    public getLights(): Light[] {
        return this.lights;
    }

    @ApiProperty({name: "users", type: UserSchema, isArray: true})
    @Expose({name: 'users'})
    public getUsers(): UserSchema[] {
        return this.users;
    }

    @ApiProperty({name: "subscription", type: Subscription})
    @Expose({name: 'subscription'})
    public getSubscription(): Subscription | null {
        return this.subscription || null;
    }

    public withLights(lights: Light[]): this {
        this.lights = lights;
        return this;
    }

    public withUsers(users: UserSchema[]): this  {
        this.users = users;
        return this;
    }

    public withSubscription(subscription: Subscription): this {
        this.subscription = subscription;
        return this;
    }
}
