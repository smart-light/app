/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class RoomService
 */
import { Injectable } from "@nestjs/common";
import { RoomRepository } from "~/module/domain/room/repository/RoomRepository";
import { RoomDto } from "~/module/domain/room/dto/RoomDto";
import { Room } from "~/module/domain/room/entity/Room";
import { LightRepository } from "~/module/domain/light/repository/LightRepository";
import { SubscriptionRepository } from "~/module/domain/subscription/repository/SubscriptionRepository";
import { UserRepository } from "~/module/domain-service/user/repository/UserRepository";

@Injectable()
export class RoomService {
    constructor(
        private readonly roomRepository: RoomRepository,
        private readonly lightRepository: LightRepository,
        private readonly subscriptionRepository: SubscriptionRepository,
        private readonly userRepository: UserRepository
    ) {}

    public async list(): Promise<Room[]> {
        return this.roomRepository.findBy();
    }

    public async create(data: RoomDto): Promise<Room> {
        const room = new Room(data.name, data.title);

        if(data.lights !== undefined) {
            const lights = await this.lightRepository.findBy({
                ids: data.lights
            });

            room.withLights(lights);
        }
        if(data.subscription !== undefined) {
            const subscription = await this.subscriptionRepository.get(data.subscription);
            room.withSubscription(subscription);
        }

        if(data.users !== undefined) {
            const users = await this.userRepository.findBy({
                ids: data.users
            });

            room.withUsers(users);
        }

        await this.roomRepository.save(room);

        return room;
    }

    public async update(id: string, data: RoomDto): Promise<Room> {
        const room = await this.roomRepository.get(id);

        if(data.lights !== undefined) {
            const lights = await this.lightRepository.findBy({
                ids: data.lights
            });

            room.withLights(lights);
        }
        if(data.subscription !== undefined) {
            const subscription = await this.subscriptionRepository.get(data.subscription);
            room.withSubscription(subscription);
        }

        if(data.users !== undefined) {
            const users = await this.userRepository.findBy({
                ids: data.users
            });

            room.withUsers(users);
        }

        await this.roomRepository.save(room);

        return room;
    }

    public async remove(id: string): Promise<void> {
        await this.roomRepository.remove(
            await this.roomRepository.get(id)
        );
    }
}
