/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class Bid
 */
import { Entity, Id, Property } from "seth-odm";
import { Expose } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

@Entity('brownie.bids')
export class Bid {
    @Id()
    @Expose()
    @ApiProperty()
    id: string;

    @Property()
    @Expose()
    @ApiProperty()
    name: string;

    @Property()
    @Expose()
    @ApiProperty()
    phone: string;

    @Property()
    @Expose()
    @ApiProperty()
    services: string[];

    @Property()
    @Expose()
    @ApiProperty()
    message: string;
}
