/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class BidController
 */
import { Body, Controller, Post } from "@nestjs/common";
import { BidService } from "~/module/brownie/service/BidService";
import { Bid } from "~/module/brownie/entity/Bid";
import { BidDto } from "~/module/brownie/dto/BidDto";
import { ApiCreatedResponse, ApiTags } from "@nestjs/swagger";

@Controller('api/v1')
@ApiTags('Brownie: Bids')
export class BidController {
    constructor(
        private readonly bidService: BidService
    ) {}


    @Post('bid')
    @ApiCreatedResponse({ type: Bid })
    public async create(@Body() dto: BidDto): Promise<Bid> {
        return this.bidService.create(dto);
    }
}
