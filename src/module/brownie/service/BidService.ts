/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class BidService
 */
import { Injectable } from "@nestjs/common";
import { BidDto } from "~/module/brownie/dto/BidDto";
import { Bid } from "~/module/brownie/entity/Bid";
import { BidRepository } from "~/module/brownie/repository/BidRepository";
import { EventEmitterInterface, InjectEventEmitter } from "nest-async-event";
import { BidCreatedEvent } from "~/module/brownie/event/BidCreatedEvent";

@Injectable()
export class BidService {

    constructor(
        private readonly bidRepository: BidRepository,
        @InjectEventEmitter() private readonly eventEmitter: EventEmitterInterface
    ) {
    }

    public async create(dto: BidDto): Promise<Bid> {
        const bid = new Bid();
        bid.phone = dto.phone;
        bid.message = dto.message;
        bid.services = dto.services;
        bid.name = dto.name;

        await this.bidRepository.save(bid);
        await this.eventEmitter.emit(new BidCreatedEvent(bid));

        return bid;
    }

}
