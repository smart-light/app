/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class BrownieModule
 */
import { HttpModule, Module } from "@nestjs/common";
import { BidController } from "~/module/brownie/controller/BidController";
import { BidRepository } from "~/module/brownie/repository/BidRepository";
import { BidService } from "~/module/brownie/service/BidService";
import { NotifyEmailListener } from "~/module/brownie/listener/NotifyEmailListener";

@Module({
    imports: [
        HttpModule,
    ],
    controllers: [
        BidController,
    ],
    providers: [
        BidRepository,
        BidService,
        NotifyEmailListener
    ]
})
export class BrownieModule {
}
