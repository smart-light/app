/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class NotifyEmailListener
 */
import { Listener, ListenerInterface } from "nest-async-event";
import { BidCreatedEvent } from "~/module/brownie/event/BidCreatedEvent";
import { MailerService } from "@nestjs-modules/mailer";
import { TelegramService } from "nestjs-telegram";
import { TelegramException } from "nestjs-telegram/dist/interfaces/telegramTypes.interface";
import { HttpService } from "@nestjs/common";

@Listener(BidCreatedEvent.name)
export class NotifyEmailListener implements ListenerInterface {

    constructor(
        private readonly mailerService: MailerService,
        private readonly telegramService: TelegramService,
        private readonly httpService: HttpService
    ) {}

    public async listen(event: BidCreatedEvent): Promise<void> {
        const to = [
            'devsinglesly@gmail.com',
            'o_ilinyh@mail.ru'
        ];

        const subject = 'Новая заявка';
        const message = `
            <div>
                <p>От кого: ${event.bid.name} <p>
                <p>Телефон: <a href="mailto:${event.bid.phone}">${event.bid.phone}</a></p>
                <p>Выбранные услуги: ${event.bid.services.join(', ')}</p>
                <p>Дополнительная информация: ${event.bid.message}</p>
            </div>
        `;

        await this.mailerService.sendMail({
            to,
            subject,
            html: message
        });

        await this.telegramMessage(event);
    }

    private async telegramMessage(event: BidCreatedEvent): Promise<void> {
        const chats = [
            362425830,
            938389006
        ];

        const message = `Новая заявка 
Имя клиента: ${event.bid.name}
Телефон: ${event.bid.phone} 
Выбранные услуги: ${event.bid.services.join(', ')}
Дополнительная информация: ${event.bid.message}
        `;

        await Promise.all(chats.map(async chat => {
            try {
                await this.telegramService.sendMessage({
                    // eslint-disable-next-line @typescript-eslint/camelcase
                    chat_id: chat,
                    text: message,
                    // eslint-disable-next-line @typescript-eslint/camelcase
                }).toPromise()
            } catch (e) {
                console.log(e);
            }
        }))
    }
}
