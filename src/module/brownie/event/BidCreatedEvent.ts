/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class BidCreatedEvent
 */
import { Bid } from "~/module/brownie/entity/Bid";
import { EventInterface } from "nest-async-event";

export class BidCreatedEvent implements EventInterface {

    public readonly name: string = BidCreatedEvent.name;

    constructor(
        public readonly bid: Bid
    ) {}

}
