/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class BidRepository
 */
import { Injectable } from "@nestjs/common";
import { Bid } from "~/module/brownie/entity/Bid";
import { DataStore } from "~/module/infrastructure/database/DataStore";

@Injectable()
export class BidRepository {

    constructor(
        private readonly dataStore: DataStore
    ) {}

    public async save(bid: Bid): Promise<void> {
        await this.dataStore.save(bid);
    }
}
