import { Id, Property } from "seth-odm";
import { Expose } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class BidDto
 */
export class BidDto {
    @Expose()
    @ApiProperty()
    name: string;

    @Expose()
    @ApiProperty()
    phone: string;

    @Expose()
    @ApiProperty({
        isArray: true,
        type: 'string'
    })
    services: string[];

    @Expose()
    @ApiProperty()
    message: string;
}
