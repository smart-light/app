/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class DataStore
 */
import { Injectable } from "@nestjs/common";
import { Collection, Db, MongoClient, ObjectId } from "mongodb";
import { ClassType, EntityOptions, IdOptions, Mapper } from "seth-odm";
import { Light } from "~/module/domain/light/entity/Light";

@Injectable()
export class DataStore {

    private readonly db: Db = this.client.db();
    private readonly mapper: Mapper = new Mapper();

    public constructor(
        private readonly client: MongoClient
    ) {}

    public getCollection(entity: ClassType<any>): Collection {
        const options: EntityOptions = Reflect.getMetadata("entity:class", entity.prototype);

        return this.db.collection(options.name);
    }

    public async findBy(cls: ClassType, filter: Record<string, any> = {}) {
        const docs = await this.getCollection(cls).find(filter).toArray();

        return docs.map(doc => this.mapper.toClass(doc, cls));
    }

    public async save(entity: any): Promise<void> {
        const doc = this.mapper.toDocument(entity).toObject();

        const id: IdOptions = Reflect.getMetadata("entity:id", entity.constructor.prototype);

        Reflect.set(entity, id.localField, String(doc._id));

        await this.getCollection(entity.constructor).insertOne(doc);
    }

    public async update(entity: any): Promise<void> {
        const doc = this.mapper.toDocument(entity).toObject();
        await this.getCollection(entity.constructor).updateOne({
            _id: doc._id
        }, doc);
    }

    public async remove(entity: any): Promise<void> {
        const doc = this.mapper.toDocument(entity).toObject();
        await this.getCollection(entity.constructor).deleteOne({_id: doc._id});
    }

    public async get<T>(id: string, cls: ClassType): Promise<T> {
        const result = await this.getCollection(cls).findOne({ _id: new ObjectId(id) });

        if(!result) {
            throw new Error(`${cls.name} not found`);
        }

        return this.mapper.toClass(result, cls);
    }
}
