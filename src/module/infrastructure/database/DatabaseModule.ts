/**
 * @package
 * @author Artem Ilinykh devsinglesly@gmail.com
 * @class DatabaseModule
 */
import { DynamicModule, Logger, Module } from "@nestjs/common";
import { MongoClient, MongoClientOptions } from "mongodb";
import { DataStore } from "~/module/infrastructure/database/DataStore";

@Module({
    providers: [
        DataStore,
    ],
    exports: [
        DataStore
    ]
})
export class DatabaseModule {
    public static forRoot(uri: string, options: MongoClientOptions): DynamicModule {
        return {
            global: true,
            module: DatabaseModule,
            providers: [
                {
                    provide: MongoClient,
                    useFactory: async () => {
                        const client = new MongoClient(uri, options);
                        await client.connect();
                        new Logger(DatabaseModule.name).log(`Connection established db is ${client.db().databaseName}`)
                        return client;
                    }
                }
            ],
            exports: [
                {
                    provide: MongoClient,
                    useFactory: c => c,
                    inject: [MongoClient]
                }
            ],
        }
    }
}
