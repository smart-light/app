build:
	docker-compose build
up:
	docker-compose up -d
log:
	docker-compose logs -f
pull:
	docker-compose pull
push:
	docker-compose push
deploy: build push
stop:
	docker-compose stop
down:
	docker-compose down --remove-orphans
shell:
	docker-compose exec $(s) $(shell)
