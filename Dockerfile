FROM node:14.4.0-alpine3.10

COPY ./package*.json /app/

WORKDIR /app


RUN npm ci

COPY . .

RUN npm run build

ENV MODE prod

CMD npm run start:${MODE}
